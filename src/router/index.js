import Vue from 'vue'
import Router from 'vue-router'
import DepositStationList from '@/components/DepositStationList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DepositStationList',
      component: DepositStationList
    }
  ]
})
