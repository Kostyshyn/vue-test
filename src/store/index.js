import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
	strict: true,
	state: {
		collections: null,
		navigation: null,
		currentState: null
	},
	mutations: {
		setCollections(state, collections){
			state.collections = collections;
		},
		setNavigation(state, navigation){
			state.navigation = navigation;
		},
		initializeCurrentState(state){
			if (localStorage.getItem('store')){
				state.currentState = JSON.parse(localStorage.getItem('store'));
			}
		},
		setCurrentState(state, current){
			if (window.localStorage){
				localStorage.setItem('store', JSON.stringify(current));
			}
			state.currentState = current;
		}
	},
	actions: {
		setCollections({ commit }, collections){
			commit('setCollections', collections);
		},
		setNavigation({ commit }, navigation){
			commit('setNavigation', navigation);
		},
		setCurrentState({ commit }, current){
			commit('setCurrentState', current);
		}
	},
	getters: {
		getCollections(state, getters){
			return state.collections;
		},
		getNavigation(state, getters){
			return state.navigation;
		},
		getCurrentState(state, getters){
			return state.currentState;
		}
	}
})
